<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::resource('press', 'PressReleaseController')->only([
    'index', 'show'
]);

Route::resource('directors', 'DirectorController')->only([
    'index', 'show'
]);

Route::get('about', 'AboutController@index')->name('about');

Route::get('faqs', 'FaqController@index')->name('faqs');

Route::resource('contact', 'ContactController')->only([
    'create', 'store'
]);

Route::resource('news', 'NewsController')->only([
    'index', 'show'
]);

Route::get('donate', 'DonateController@index')->name('donate');
Route::post('send-donation', 'DonateController@donate')->name('send-donation');

Route::resource('learning-resources', 'LearningResourceController')->only([
    'index', 'show'
]);
