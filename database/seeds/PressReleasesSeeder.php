<?php

use App\PressReleases;
use Illuminate\Database\Seeder;

class PressReleasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PressReleases::class, 20)->create();
    }
}
