<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DirectorSeeder::class);
        $this->call(PressReleasesSeeder::class);
        $this->call(DonationSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(SpokesPeersonSeeder::class);
        $this->call(LearningResourceSeeder::class);
        $this->call(FormSeeder::class);
    }
}