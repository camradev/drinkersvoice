<?php

use App\SpokesPerson;
use Illuminate\Database\Seeder;

class SpokesPeersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SpokesPerson::class, 10)->create();
    }
}
