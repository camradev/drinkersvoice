<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Director;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Director::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'role' => $faker->jobTitle(),
        'email' => $faker->unique()->safeEmail,
        'bio' => $faker->realText(),
        'photo' => $faker->imageUrl(500,500)
    ];
});
