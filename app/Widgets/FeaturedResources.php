<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\LearningResource;

class FeaturedResources extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $resources = LearningResource::get()->take(3);
        return view('widgets.featured_resources', [
            'resources' => $resources,
        ]);
    }
}
