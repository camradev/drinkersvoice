<?php

namespace App\Widgets;

use App\FeaturedSlider;
use Arrilot\Widgets\AbstractWidget;

class HomeSlider extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $slides = FeaturedSlider::get();

        return view('widgets.home_slider', [
            'config' => $this->config,
            'slides' => $slides,
        ]);
    }
}
