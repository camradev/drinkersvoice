<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Donation;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class DonateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the about view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['more_news'] = News::orderBy('created_at', "DESC")->limit(3)->get();
        return view('donate', $data);
    }

    /**
     * Create donation and send to stripe
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function donate(Request $request)
    {
        $validatedData = $request->validate([
            'fullname'    => 'required',
            'email'       => 'required|email',
            'amount'      => 'required',
        ]);

        Stripe::setApiKey("sk_test_AiAo2zL9qyugznrZjsAS5Kv5");
        $customer = Customer::create([
            'email'  => request('email'),
            'name'   => request('fullname'),
            'source' => request('stripeToken')
        ]);

        $charge = \Stripe\Charge::create([
            'amount'   => (request('amount') * 100),
            'currency' => 'gbp',
            'customer' => $customer->id,
        ]);

        if($charge->status=='succeeded'){
            $insert = [
                'amount'         => request('amount'),
                'stripe_receipt' => $charge->id
            ];
            Donation::create($insert);
            return redirect()->back()->with('success', 'Thank you for your donation');
        }
        else{
            return redirect()->back()->withErrors(['Sorry there was an error. Please try again']);
        }
    }
}
