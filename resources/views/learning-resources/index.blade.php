@extends('layouts.app')

@section('content')
    <section class="container py-4">
        <h1>News</h1>
        @if($resources)
            @foreach($resources as $item)
                <div class="news-item border-bottom pb-3 mb-3">
                    <a href="{{ route('learning-resources.show', $item->id) }}" class="row">
                        <div class="col-lg-3">
                            <img src="{{ $item->image }}" class=" w-100 img-fluid">
                        </div>
                        <div class="col-lg-9">
                            <h5>{{ $item->title }}</h5>
                            <p>{{ str_limit($item->content, 300, '...') }}</p>
                            <span class="btn btn-primary">Read More</span>
                        </div>
                    </a>
                </div>
            @endforeach
            {{ $resources->links() }}
        @endif
    </section>
@endsection