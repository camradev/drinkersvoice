@extends('layouts.app')

@section('content')
    <div class="header-image d-flex" style="background: url('{{ $resource->image }}') no-repeat center center; background-size: cover; height: 350px;">
        <div class="container align-self-center">
            <h1 class="mb-0">{{ $resource->title }}</h1>
        </div>
    </div>
    <section class="container py-4">
        <div class="row">
            <article class="col-lg-8">
                <p>{{ $resource->content }}</p>
            </article>
            <aside class="col-lg-3">
                <h5>More News</h5>
                @if($more_resources)
                    @foreach($more_resources as $item)
                        <a href="{{ route('learning-resources.show', $item->id) }}" class="card mb-3">
                            <img src="{{ $item->image }}" class="card-image-top w-100 img-fluid">
                            <div class="card-body">
                                <h5 class="mb">{{ $item->title }}</h5>
                                <p>{{ str_limit($item->content, 50, '...') }}</p>
                                <span class="btn btn-primary">Read More</span>
                            </div>
                        </a>
                    @endforeach
                @endif
            </aside>
        </div>
    </section>
@endsection