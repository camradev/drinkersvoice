@extends('layouts.app')

@section('content')
    <section class="container py-4">
        <h1>Press</h1>
        @if($press_releases)
            <div class="row">
                @foreach($press_releases as $press)
                    <div class="col-lg-4 mb-4">
                        <a href="{{ route('press.show', $press->id) }}" class="card">
                            <img src="{{ $press->image }}" class="card-image-top w-100 img-fluid">
                            <div class="card-body">
                                <h5 class="mb-0">{{ $press->title }}</h5>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            {{ $press_releases->links() }}
        @endif
    </section>
@endsection