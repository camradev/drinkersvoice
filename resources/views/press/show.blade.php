@extends('layouts.app')

@section('content')
    <div class="header-image d-flex" style="background: url('{{ $press_release->image }}') no-repeat center center; background-size: cover; height: 350px;">
        <div class="container align-self-center">
            <h1 class="mb-0 press-title-white">{{ $press_release->title }}</h1>
        </div>
    </div>
    <section class="container py-4">
        <div class="row">
            <article class="col-lg-9">
                <p>{{ $press_release->content }}</p>
            </article>
            <aside class="col-lg-3">
                <h5>More Press</h5>
                @if($more_press)
                    @foreach($more_press as $press)
                        <a href="{{ route('press.show', $press->id) }}" class="card mb-3">
                            <img src="{{ $press->image }}" class="card-image-top w-100 img-fluid">
                            <div class="card-body">
                                <h5 class="mb">{{ $press->title }}</h5>
                                <p>{{ str_limit($press->content, 50, '...') }}</p>
                                <span class="btn btn-primary">Read More</span>
                            </div>
                        </a>
                    @endforeach
                @endif
            </aside>
        </div>
    </section>
@endsection
