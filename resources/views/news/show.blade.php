@extends('layouts.app')

@section('content')
    <div class="header-image d-flex" style="background: url('{{ $news->image }}') no-repeat center center; background-size: cover; height: 350px;">
        <div class="container align-self-center">
            <h1 class="mb-0">{{ $news->title }}</h1>
        </div>
    </div>
    <section class="container py-4">
        <div class="row">
            <article class="col-lg-8">
                <p>{{ $news->content }}</p>
            </article>
            <aside class="col-lg-3">
                <h5>More News</h5>
                @if($more_news)
                    @foreach($more_news as $news_item)
                        <a href="{{ route('news.show', $news_item->id) }}" class="card mb-3">
                            <img src="{{ $news_item->image }}" class="card-image-top w-100 img-fluid">
                            <div class="card-body">
                                <h5 class="mb">{{ $news_item->title }}</h5>
                                <p>{{ str_limit($news_item->content, 50, '...') }}</p>
                                <span class="btn btn-primary">Read More</span>
                            </div>
                        </a>
                    @endforeach
                @endif
            </aside>
        </div>
    </section>
@endsection