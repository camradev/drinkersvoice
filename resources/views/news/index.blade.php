@extends('layouts.app')

@section('content')
    <section class="container py-4">
        <h1>News</h1>
        @if($news)
            @foreach($news as $news_item)
                <div class="news-item border-bottom pb-3 mb-3">
                    <a href="{{ route('news.show', $news_item->id) }}" class="row">
                        <div class="col-lg-3">
                            <img src="{{ $news_item->image }}" class=" w-100 img-fluid">
                        </div>
                        <div class="col-lg-9">
                            <h5>{{ $news_item->title }}</h5>
                            <p>{{ str_limit($news_item->content, 300, '...') }}</p>
                            <span class="btn btn-primary">Read More</span>
                        </div>
                    </a>
                </div>
            @endforeach
            {{ $news->links() }}
        @endif
    </section>
@endsection