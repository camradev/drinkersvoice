@extends('layouts.app')

@section('content')
    <section class="container py-4">
            <h1>About Us</h1>
            About Drinkers’ Voice
            <p>Do you enjoy opening a bottle of wine at home after a long week at work or a pint in your local pub with friends on a sunny afternoon? How about the odd night out to celebrate a birthday, work promotion or other happy news? If so, we say embrace it!</p>

            <p>Drinkers’ Voice is a new network of people across the UK who enjoy drinking moderately as part of a healthy lifestyle. We want to bring normal people back into the debate about drinking to counter anti-alcohol campaigners who want to stop you enjoying a drink.</p>
    </section>
    <div class="container">
        @widget('featuredResources')
    </div>
@endsection
