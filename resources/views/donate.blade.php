@extends('layouts.app')

@section('content')
    <section class="container py-4">
        <div class="row">
            <form method="POST" class="col-lg-8" id="payment-form" action="{{ route('send-donation') }}">
                <h1>Donate</h1>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @csrf
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" name="fullname" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>Email Address</label>
                    <input type="email" name="email" class="form-control" required />
                </div>
                <div class="form-group">
                    <div id="card-element">
                        <!-- A Stripe Element will be inserted here. -->
                    </div>
                    <!-- Used to display Element errors. -->
                    <div id="card-errors" role="alert"></div>
                </div>

                <div class="form-group">
                    <label class="d-block">Donation Amount</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">&pound;</div>
                        </div>
                        <input type="number" name="amount" class="form-control" required />
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Donate!</button>
            </form>
            <div class="col-lg-3">
                <h5>Recent News</h5>
                @if($more_news)
                    @foreach($more_news as $news_item)
                        <a href="{{ route('news.show', $news_item->id) }}" class="card mb-3">
                            <img src="{{ $news_item->image }}" class="card-image-top w-100 img-fluid">
                            <div class="card-body">
                                <h5 class="mb">{{ $news_item->title }}</h5>
                                <p>{{ str_limit($news_item->content, 50, '...') }}</p>
                                <span class="btn btn-primary">Read More</span>
                            </div>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript">
        var stripe = Stripe('pk_test_RkMLHyGZFUsGo4xozZT5oxQN');
        var elements = stripe.elements();
        var style = {
            base: {
                fontSize: '16px',
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Create a token or display an error when the form is submitted.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the customer that there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }
    </script>
@endsection
