<!-- Slider main container -->
<div class="swiper-container home-slider">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        @foreach($slides as $slide)
        <div class="swiper-slide" style="background-image: url('/storage/{{$slide->image}}')">
            <div class="content">
                <h1>{{ $slide->title }}</h1>
                <p>{{ $slide->caption }}</p>
                <p><a href="{{ $slide->link }}" class="btn btn-primary btn-lg">Find out more</a></p>
            </div>            
        </div>
        @endforeach
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev">
        <i class="material-icons">
            chevron_left
        </i>
    </div>
    <div class="swiper-button-next">
        <i class="material-icons">
        chevron_right
        </i>
    </div>
</div>
