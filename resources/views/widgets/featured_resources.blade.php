<div class="featured-resources">
    <div class="container text-center">
        <h2>Featured Resources</h2>
        <p>Find out free resources, read and learn more</p>

        <div class="row">
            @foreach($resources as $resource)
                <a href="#" class="col-lg-4 resource-item">
                    <div class="image" style="background-image: url('/storage/{{ $resource->image }}')"></div>
                    <div class="content">
                        <h3>{{ $resource->title }}</h3>
                        <p>Quo consulatu voluptaria interesset at, nonumy quodsi detracto usu ut. Error offendit est eu. Et altera suavitate his. Nusquam platonem disputationi sed et, movet…</p>
                        <span>Learn more</span>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
</div>
