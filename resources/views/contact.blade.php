@extends('layouts.app')

@section('content')
    <section class="container py-4">
        <div class="row">
            <form class="col-lg-9" method="POST" action="{{ route('contact.store') }}">
                <h1>Contact Us</h1>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @csrf
                <div class="form-group">
                    <label class="label-text">Email Address</label>
                    <input type="email" name="from_email" class="form-control" value="{{ old('from_email') }}" />
                </div>
                <div class="form-group">
                    <label class="label-text">Message</label>
                    <textarea name="content" class="form-control">{{ old('content') }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
            <div class="col-lg-3">
                Contact Info to go here
            </div>
        </div>
    </section>
    @widget('featuredResources')
@endsection
