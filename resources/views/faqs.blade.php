@extends('layouts.app')

@section('content')
    <section class="container py-4">
        <h1>FAQs</h1>

        @foreach ($faqs as $faq)
        <div class="faq-container">
            <h2>{{ $faq->question }}</h2>
            <p>{{ $faq->answer }}</p>
        </div>
        @endforeach

    </section>
    <div class="container">
        @widget('featuredResources')
    </div>
@endsection
