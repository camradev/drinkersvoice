@extends('layouts.app')

@section('content')
    <div class="header-image d-flex" style="background: url('{{ $director->photo }}') no-repeat center center; background-size: cover; height: 350px;">
        <div class="container align-self-center">
            <h1 class="mb-0">Our Directors</h1>
        </div>
    </div>
    <section class="container py-4">
        <div class="row">
            <div class="col-lg-4">
                <img src="{{ $director->photo }}" class="w-100 img-fluid" />
            </div>
            <div class="col-lg-8">
                <h2>{{ $director->first_name . ' ' . $director->last_name }}</h2>
                <strong class="mb-3 d-block">{{ $director->role }}</strong>
                <p>{{ $director->bio }}</p>
                <a href="mailto:{{ $director->email }}" class="btn btn-primary">Contact {{ $director->first_name }}</a>
            </div>
        </div>
    </section>
@endsection