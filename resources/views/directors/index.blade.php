@extends('layouts.app')

@section('content')
    <section class="container py-4">
        <h1>Directors</h1>
        @if($directors)
            <div class="row">
                @foreach($directors as $director)
                    <div class="col-lg-4 mb-4">
                        <a href="{{ route('directors.show', $director->id) }}" class="card">
                            <img src="/storage/{{ $director->photo }}" class="card-image-top w-100 img-fluid">
                            <div class="card-body">
                                <h5 class="mb-0">{{ $director->first_name . ' ' . $director->last_name }}</h5>
                                <span>{{ $director->role }}</span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        @endif
    </section>
@endsection
