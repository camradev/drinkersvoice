@extends('layouts.app')

@section('content')
    <div class="container">
        @widget('homeSlider')

        <div class="intro-section">
            <p>Drinkers’ Voice is a new network of people across the UK who enjoy drinking moderately as part of a healthy lifestyle.</p>
            <p class="small">We want to bring normal people back into the debate about drinking to counter anti-alcohol campaigners who want to stop you enjoying a drink.</p>
        </div>
        @widget('featuredResources')
    </div>
@endsection
