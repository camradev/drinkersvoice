<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <h5>Useful Links</h5>
                <ul class="nav flex-column">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('press.index') }}">Press</a></li>
                    <li><a href="{{ route('about') }}">About</a></li>
                    <li><a href="{{ route('directors.index') }}">Directors</a></li>
                    <li><a href="{{ route('contact.create') }}">Contact Us</a></li>
                </ul>
            </div>            
            <div class="col-lg-3">
                <h5>Recent Press</h5>
                <ul class="nav flex-column">
                    @foreach($footer_press_releases as $press)
                        <li><a href="{{ route('press.show', $press->id) }}">{{ $press->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-3  social-links">
                <h5>Follow Us</h5>
                <p>Stay in touch and see the latest from Drinkers Voice</p>
                <ul class="nav">
                   <li><a href="https://www.facebook.com/DrinkersVoiceUK/"><i class="fab fa-facebook-f"></i></a></li>
                   <li><a href="https://twitter.com/DrinkersVoiceUK"><i class="fab fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h5>Get In Touch</h5>
                <ul class="nav flex-column">
                    <li><a href="#">voice@drinkersvoice.org.uk</a></li>
                </ul>
            </div>

            <a href="#" class="back-to-top"><i class="fas fa-chevron-circle-up"></i>
                <br/>Back to <br/>top
            </a>
        </div>
    </div>

   
    <div class="terms">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <ul>
                        <li class="copyright"> &copy; Copyright Drinkers Voice {{ date('Y') }}</li>
                        <li><a href="#" class="nav-link">Terms &amp; Conditions</a></li>
                        <li><a href="#" class="nav-link">Privacy Policy</a></li>              
                    </ul>
                </div>

                <div class="col-lg-3">
                    <a href="/"><img src="{{ asset('img/drinkersvoice-white.png') }}" class="footer-logo" /></a>
                </div>
            </div>
        </div>
    </div>
</footer>
