<header class="header">
    <div class="container">
        <a href="{{ route('home') }}">
            <img src="{{ asset('img/drinkersvoice.png') }}" class="logo" alt="drinkers voice"/>
        </a>
            
        <nav class="main-nav">
            <li>
                <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('faqs') }}">FAQs</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('press.index') }}">Press</a></li>
                <li class="nav-item">
                    <a class="nav-link" href="#">People <i class="material-icons desktop-dropdown-icon">keyboard_arrow_down</i></a>
                    <ul>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('directors.index') }}">Directors</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('directors.index') }}">Spokespeople</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item"><a class="nav-link" href="{{ route('contact.create') }}">Contact Us</a></li>
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">
                            <i class="material-icons">person</i>
                            {{ __('Login') }}
                            <i class="material-icons desktop-dropdown-icon">keyboard_arrow_down</i>
                        </a> 
                        @if (Route::has('register'))
                        <ul>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">
                                    <i class="material-icons">person_add</i>
                                    {{ __('Register') }}
                                </a>
                            </li>
                        </ul>
                        
                    @endif
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="material-icons">person</i>
                            {{ Auth::user()->name }} <i class="material-icons desktop-dropdown-icon">keyboard_arrow_down</i>
                        </a>
                        <ul>
                            <li class="nav-item"><a class="nav-link" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="material-icons">exit_to_app</i>
                                Logout</a></li>
                        </ul>
                        
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
                <li class="nav-item highlight-nav"><a class="nav-link" href="{{ route('donate') }}">Donate</a></li>
            </ul>
        </nav>
        
        <span id="moby-button">
            <i class="material-icons">menu</i> 
            Menu
        </span>

    </div>
</header>
