## Installation
1. Clone the repository

2. Install composer and run ``composer install`` (https://getcomposer.org/download/)

3. Run ``composer install``

4. Copy contents of ``.env.example`` and place in a new ``.env`` file.

7. Run ``php artisan migrate`` to create tables

8. Run ``php artisan db:seed`` to fill tables with default data

9. php artisan config:clear

## Compiling assets

1. Run ``npm run watch`` in the terminal and make changes to the scss and js files in ``resources``

2. For `production` run `npm run production` before committing files

## Resources for reference
- Icons to use: https://material.io/tools/icons/?icon=3d_rotation&style=baseline
- Mobile Nav: http://www.joshuasanger.ca/libraries/moby/