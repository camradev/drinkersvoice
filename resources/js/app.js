/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Moby from './Moby';
import Swiper from 'swiper';

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

$(function(){

    // Mobile Menu
    var mobyMenu = new Moby({
        menu       : $('.main-nav'), // The menu that will be cloned
        mobyTrigger: $('#moby-button'), // Button that will trigger the Moby menu to open
        subMenuOpenIcon: '<i class="material-icons">keyboard_arrow_down</i>', // icon that shows when the submenu is hidden
        subMenuCloseIcon: '<i class="material-icons">keyboard_arrow_up</i>', 
    });

    // scroll to top 
    $('.back-to-top').on('click', function(e){
        e.preventDefault();

        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    })

    // Home slider
    //initialize swiper when document ready
    var mySwiper = new Swiper ('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },
      })
});

